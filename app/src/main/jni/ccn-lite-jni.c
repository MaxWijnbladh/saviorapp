// src/android/jni/ccn-lite-jni.c

#include <string.h>
#include <jni.h>
#include <android/log.h>

int jni_bleSend(unsigned char *data, int len);

#include "../../ccn-lite-android.c"

static JavaVM *jvm;
static jclass ccnLiteClass;
static jobject ccnLiteObject;

/*Init Relay*/

JNIEXPORT jstring JNICALL
Java_ch_unibas_ccn_1lite_1android_MainActivity_androidPeek(JNIEnv* env,
                                                             jobject thiz, jstring ipString, jint portString, jstring prefixString)
{
    char buf[128];

    const char *ip = (*env)->GetStringUTFChars(env, ipString, 0);
    int port = (int) portString;
    const char *prefix = (*env)->GetStringUTFChars(env, prefixString, 0);

    return (*env)->NewStringUTF(env, ccnl_android_peek("ndn2013", ip, port, prefix));
}

JNIEXPORT jstring JNICALL
Java_ch_unibas_ccn_1lite_1android_SensorsAdapter_androidPeek(JNIEnv* env,
                                                           jobject thiz, jstring ipString, jint portString, jstring prefixString)
{
    char buf[128];

    const char *ip = (*env)->GetStringUTFChars(env, ipString, 0);
    int port = (int) portString;
    const char *prefix = (*env)->GetStringUTFChars(env, prefixString, 0);

    return (*env)->NewStringUTF(env, ccnl_android_peek("ndn2013", ip, port, prefix));
}



/**
  * @desc connect to node with specific ip addr and send interest object
  * @param string $ipString - ip addr,integer-$portString-port,string -$contentString- name of interest object
  * @return String - content Object
*/



void jni_append_to_log(char *line)
{
    JNIEnv *env;
    int len = strlen(line);

    if (len > 0 && line[len - 1] == '\n')
        line[len - 1] = '\0';

    (*jvm)->GetEnv(jvm, (void**)&env, JNI_VERSION_1_4);
    jmethodID method = (*env)->GetMethodID(env, ccnLiteClass,
                                           "appendToLog",
                                           "(Ljava/lang/String;)V");
    (*env)->CallVoidMethod(env, ccnLiteObject, method,
                           (*env)->NewStringUTF(env, line));
}


