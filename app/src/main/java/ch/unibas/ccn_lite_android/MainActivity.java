package ch.unibas.ccn_lite_android;

import android.app.ActionBar;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Handler;

import org.joda.time.DateTime;
import org.json.*;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private Toolbar toolbar;
    private RecyclerView recyclerView;
    RecyclerView.LayoutManager mLayoutManager;
    private SensorsAdapter adapter;
    private List<ch.unibas.ccn_lite_android.Sensor> sensorList;
    private SwipeRefreshLayout swipeContainer;

    String sensorsList = "";
    String sensorList_prefix = "/sds/sensors";
    String localIP = "127.0.0.1";
    int relayPort = 9695;
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        swipeContainer.setOnRefreshListener(this);
        initializeSwipeRefresh(swipeContainer);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        sensorList = new ArrayList<>();
        adapter = new SensorsAdapter(this, sensorList);

        mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(adapter);
        getSensors();
        //addSensors();


        new SDSPeekTask().execute(localIP, String.valueOf(relayPort), sensorList_prefix);
        //TextView sensorlistText = (TextView)findViewById(R.id.sensorlist);
        //sensorlistText.setText(sensorsList);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_items, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {

            case R.id.action_refresh:
                refresh();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }



    private void refresh() {
        Log.d(TAG, "action pressed!");
        Log.d(TAG, "Before refresh " + sensorList);
        sensorList.clear();
        Log.d(TAG, "While refresh " + sensorList);
        getSensors();
        //addSensors();
        Log.d(TAG, "After refresh " + sensorList);
    }

    private void addSensors() {
        String prefix = "/p/office/sensor1";
        String[] prefixParts = prefix.split("/");
        String mac = prefixParts[prefixParts.length-1];
        Sensor s = new ch.unibas.ccn_lite_android.Sensor(mac, "/p/office/sensor1",10, "Office", 4214135);
        s.setActive(true);
        sensorList.add(s);
        s = new ch.unibas.ccn_lite_android.Sensor("Sensor 2", "/p/office/sensor2",10, "Office", 4214135);
        s.setActive(false);
        sensorList.add(s);

        adapter.notifyDataSetChanged();
    }

    private void initializeSwipeRefresh(SwipeRefreshLayout swipe) {
        swipe.setColorSchemeResources(R.color.colorPrimary);
    }


    @Override public void onRefresh() {
        refresh();
    }


    private void getSensors() {
        new SDSPeekTask().execute(localIP, String.valueOf(relayPort), sensorList_prefix);
    }


    private class SDSPeekTask extends AsyncTask<String, Void, String> {

        protected String doInBackground(String... params) {
            String ipString = params[0];
            int portInt = Integer.parseInt(params[1]);
            String prefix = params[2];
            return androidPeek(ipString, portInt, prefix);
        }

        protected void onPostExecute(String result) {
            Log.d(TAG, "peek done!");

            try {
                JSONArray sensors = new JSONArray(result);
                Log.d(TAG, "sensors= " + sensors);
                for (int i = 0; i < sensors.length(); i++) {
                    JSONObject sensor = sensors.getJSONObject(i);
                    Log.d(TAG, "hIJO ARRAY " + sensor);
                    String prefix = sensor.getString("pf");
                    String[] prefixParts = prefix.split("/");
                    String mac = prefixParts[prefixParts.length-1];
                    Boolean active = Boolean.valueOf(sensor.getString("a"));
                    int looptime = Integer.parseInt(sensor.getString("but"));
                    long initTime = (long) sensor.getDouble("bt");
                    String location = sensor.getString("lo");

                    Sensor s = new ch.unibas.ccn_lite_android.Sensor(mac, prefix,looptime, location, initTime);
                    s.setActive(active);
                    Log.d(TAG, "Active: " + s.getActive());
                    sensorList.add(s);
                    adapter.notifyDataSetChanged();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            swipeContainer.setRefreshing(false);
        }


    }


    // JNI declarations
    public native String androidPeek(String ipString, int port, String prefix);


    static {
        System.loadLibrary("ccn-lite-android");
    }
}


