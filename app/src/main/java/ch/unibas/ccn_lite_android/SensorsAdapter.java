package ch.unibas.ccn_lite_android;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.nfc.Tag;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.graphics.drawable.GradientDrawable;

import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.codetroopers.betterpickers.radialtimepicker.RadialTimePickerDialogFragment;
import com.codetroopers.betterpickers.calendardatepicker.MonthAdapter;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.w3c.dom.Text;

import java.util.Calendar;
import java.util.List;

/**
 * Created by Andrew on 2016-12-20.
 */

public class SensorsAdapter extends RecyclerView.Adapter<SensorsAdapter.MyViewHolder> implements
        CalendarDatePickerDialogFragment.OnDateSetListener, RadialTimePickerDialogFragment.OnTimeSetListener {

    private Context mContext;
    private List<ch.unibas.ccn_lite_android.Sensor> sensorList;
    private static final String TAG = "MainActivity";
    private String date;
    String localIP = "127.0.0.1";
    int relayPort = 9695;
    public CharSequence OK = "OK";
    public String sensorResult;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView prefix_header;
        public TextView prefix_text;
        public ImageView status;
        public Button action_1;
        public Button action_2;
        public TextView temp;
        public TextView light;
        public TextView humidity;
        public TextView header;

        public MyViewHolder(View view) {
            super(view);
            header = (TextView) view.findViewById(R.id.info_text);
            status = (ImageView) view.findViewById(R.id.sensor_status);
            prefix_header = (TextView) view.findViewById(R.id.prefix_header);
            prefix_text = (TextView) view.findViewById(R.id.prefix_text);
            action_1 = (Button) itemView.findViewById(R.id.action1);
            action_2 = (Button) itemView.findViewById(R.id.action2);
            temp = (TextView) view.findViewById(R.id.readings_temp);
            light = (TextView) view.findViewById(R.id.readings_light);
            humidity = (TextView) view.findViewById(R.id.readings_hum);
        }
    }


    public SensorsAdapter(Context mContext, List<ch.unibas.ccn_lite_android.Sensor> sensorList) {
        this.mContext = mContext;
        this.sensorList = sensorList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ch.unibas.ccn_lite_android.Sensor sensor = sensorList.get(position);
        holder.prefix_text.setText(sensor.getPrefix());
        holder.header.setText(sensor.getHeader());
        Log.d(TAG, "sensor active: " + sensor.getActive());
        if (!sensor.getActive()) {
            holder.action_1.setEnabled(false);
            holder.action_1.setAlpha(0.3f);
            //holder.status.setImageResource(R.drawable.ic_status_red);
            GradientDrawable gd = (GradientDrawable) holder.status.getBackground().getCurrent();
            gd.setColor(Color.parseColor("#FF0000"));
        }
        holder.action_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int seqno = calcSeqno(sensor.getPrefix(), sensor.getLooptime(), sensor.getInitTime());
                peekSensor(sensor.getPrefix(), seqno);
            }
        });

        holder.action_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateTime now = DateTime.now();
                MonthAdapter.CalendarDay maxDate = new MonthAdapter.CalendarDay(now.getYear(), now.getMonthOfYear() - 1, now.getDayOfMonth());
                CalendarDatePickerDialogFragment cdp = new CalendarDatePickerDialogFragment()
                        .setDateRange(null, maxDate)
                        .setOnDateSetListener(SensorsAdapter.this);
                cdp.show(((FragmentActivity) mContext).getSupportFragmentManager(), String.valueOf(position));
            }
        });
    }

    @Override
    public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {
        Log.d(TAG, dialog.getTag());
        Log.d(TAG, String.valueOf(year) + " " + String.valueOf(monthOfYear) + " " + String.valueOf(dayOfMonth));
        date = String.valueOf(year) + "/" + String.valueOf(monthOfYear + 1) + "/" + String.valueOf(dayOfMonth);

        RadialTimePickerDialogFragment rtpd = new RadialTimePickerDialogFragment()
                .setOnTimeSetListener(SensorsAdapter.this);
        rtpd.show(((FragmentActivity) mContext).getSupportFragmentManager(), dialog.getTag());
    }

    @Override
    public void onTimeSet(RadialTimePickerDialogFragment dialog, int hourOfDay, int minute) {
        date += " " + String.valueOf(hourOfDay) + ":" + String.valueOf(minute);
        Log.d(TAG, date);
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy/MM/dd H:m");
        DateTime dt = formatter.parseDateTime(date);
        Sensor sensor = sensorList.get(Integer.parseInt(dialog.getTag()));
        int seqno = calcHisSeqno(dt, sensor.getLooptime(), sensor.getInitTime());
        Log.d(TAG, String.valueOf("tag: " + dialog.getTag()));
        Log.d(TAG, String.valueOf("looptime: " + sensor.getLooptime()));
        Log.d(TAG, String.valueOf("init time: " + sensor.getInitTime()));
        Log.d(TAG, String.valueOf("seqno: " + seqno));
        Log.d(TAG, String.valueOf(dt.getMillis() / 1000));
        if (seqno < 0) {
            Log.d(TAG, "Asking for values before the sensor was initiazlied");
        } else {
            peekSensor(sensor.getPrefix(), seqno);
        }
    }

    private void peekSensor(String prefix, int seqno) {
        Log.d(TAG, "seqno " + seqno);
        //sensorResult = androidPeek(localIP, relayPort, prefix + "/" + seqno);
        Log.d(TAG, "peeking prefix: " + prefix + "/" + seqno);
        new SensorPeekTask().execute(localIP, String.valueOf(relayPort), prefix + "/" + seqno);
        Log.d(TAG, "sensorresult: " + sensorResult);

        //sensorResult = "65-10-0.24-27.750-9.61";
    }

    private int calcHisSeqno(DateTime selectedTime, int looptime, long initTime) {
        long nowTime = selectedTime.getMillis() / 1000;
        long diffTime = nowTime - initTime;
        int seqno = (int) Math.floor(diffTime / looptime) - 1;
        if (seqno % 15 == 0) {
            seqno -= 1;
        }
        return seqno;
    }

    private int calcSeqno(String prefix, int looptime, long initTime) {
        long nowTime = new DateTime().getMillis() / 1000;
        long diffTime = nowTime - initTime;
        int seqno = (int) Math.floor(diffTime / looptime) - 1;
        if (seqno % 15 == 0) {
            seqno -= 1;
        }
        return seqno;
    }

    public native String androidPeek(String ipString, int port, String prefix);

    private class SensorPeekTask extends AsyncTask<String, Void, String> {

        protected String doInBackground(String... params) {
            String ipString = params[0];
            int portInt = Integer.parseInt(params[1]);
            String prefix = params[2];
            return androidPeek(ipString, portInt, prefix);
        }

        protected void onPostExecute(String result) {
            Log.d(TAG, "peek done!");
            Log.d(TAG, "result: " + result);


            try {
                String[] parts = result.split("-");
                double light = Double.parseDouble(parts[2]);
                double temperature = Double.parseDouble(parts[3]);
                double humidity = Double.parseDouble(parts[4]);
                Log.d(TAG, "i AM VALUES " + "Light " + light + " I am temp " + temperature + " I am humidity " + humidity);
                View view = View.inflate(mContext, R.layout.dialog, null);


                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(mContext);
                View v = ((Activity) mContext).getLayoutInflater().inflate(R.layout.dialog, null);
                alertBuilder.setView(v);
                TextView t = (TextView) v.findViewById(R.id.Temperature);
                t.setText("Temperature: " + temperature + " C");
                TextView h = (TextView) v.findViewById(R.id.Humidity);
                h.setText("Humidity: " + humidity + "%");
                TextView l = (TextView) v.findViewById(R.id.Light);
                l.setText("Light: " + light + " Lux");
                alertBuilder.setPositiveButton("Close", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                AlertDialog alert = alertBuilder.create();
                alert.show();
                Button alertButton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
                alertButton.setTextColor(v.getResources().getColor(R.color.colorPrimary));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }

    @Override
    public int getItemCount() {
        return sensorList.size();
    }
}

