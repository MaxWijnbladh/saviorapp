package ch.unibas.ccn_lite_android;

/**
 * Created by Andrew on 2016-12-20.
 */

public class Sensor {

    private String prefix;
    private double light;
    private double humidity;
    private double temparature;
    private int looptime;
    private String location;
    private long initTime;
    private Boolean active;
    private String header;

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public Sensor(String header, String prefix, int looptime, String location, long initTime) {
        this.prefix = prefix;
        this.looptime = looptime;
        this.location = location;
        this.initTime = initTime;
        this.header = header;
        this.active = false;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getPrefix() {
        return prefix;
    }

    public double getLight() {
        return light;
    }

    public double getTemparature() {
        return temparature;
    }

    public double getHumidity() {
        return humidity;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }
    public void setLight(double light) {
        this.light = light;
    }

    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }

    public void setTemparature(double temparature) {
        this.temparature = temparature;
    }

    public int getLooptime() {
        return looptime;
    }

    public void setLooptime(int looptime) {
        this.looptime = looptime;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public long getInitTime() {
        return initTime;
    }

    public void setInitTime(long initTime) {
        this.initTime = initTime;
    }
}
